'use strict';

class MySql {

	constructor() {
		let mysql = require('mysql');

		this.connection = mysql.createConnection({
  			host: '127.0.0.1',
  			user: process.env.MYSQL_USER || 'root',
  			password: process.env.MYSQL_PASSWORD || 'root',
  			database: process.env.MYSQL_DATABASE || 'shop_es'
		});

		this.connection.on('end', function(error) {
			console.info(error);
			
			if (error.code == 'PROTOCOL_CONNECTION_LOST') {
				// try to reconnect
				this.connection.connect();
			}
		});

		this.connection.connect();
	}

	query(sql, params) {
		return new Promise((resolve, reject) => {

			console.info(sql, params || '');

			this.connection.query(sql, params, function (err, rows, fields) {
//				console.error(err);

				if (err) {
					reject(err);
					console.error(err);
				}
				else {
 					resolve(rows, fields);
				}
			});
		});
	}
}

module.exports = new MySql();