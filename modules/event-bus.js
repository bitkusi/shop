'use strict';

class EventBus {
	constructor() {
		this.exchange = 'logs';

		let amqp = require('amqplib');
		let busHost = process.env.OPENSHIFT_RABBITMQ_SERVICE_HOST || 'localhost';

		this.conn = amqp.connect('amqp://' + busHost).catch(console.error);
	}

	publish(key, message) {
		let self = this;
		message = JSON.stringify(message || {});

		if (this.channel) {
//			console.log(key);
			this.channel.publish(this.exchange, key, new Buffer(message));
		}
		else {
			this.conn.then(function (conn) {
				conn.createChannel().then(function (ch) {
					ch.assertExchange(self.exchange, 'topic', { durable: false });

//					console.log(key);
					ch.publish(self.exchange, key, new Buffer(message));
					self.channel = ch;
				});
			});
		}
	}

	subscribe(key, callback) {
		let self = this;

		function onMessage(msg) {
			let content = JSON.parse(msg.content.toString());
			callback(msg.fields.routingKey, content);
		}

		this.conn.then(function (conn) {
			conn.createChannel().then(function (ch) {
				ch.assertExchange(self.exchange, 'topic', { durable: false });
				ch.assertQueue('', { exclusive: true })
					.then(function (qok) {
						return ch.bindQueue(qok.queue, self.exchange, key).then(function () {
							return qok.queue;
						});
					})
					.then(function (queue) {
						return ch.consume(queue, onMessage, { noAck: true });
					});
			});
		});
	}
}

module.exports = new EventBus();