'use strict';

const bus = require('../modules/event-bus');
const db = require('../modules/database');
const Slack = require('slack-node');

const slack = new Slack();
const webhookUri = "https://hooks.slack.com/services/XXX";
slack.setWebhook(webhookUri);

function send(mandant, id) {
	return db.query('SELECT * FROM product WHERE id = ?', [id]);
}

bus.subscribe('command.*', (key, content) => {

	switch (key) {
		case 'command.addToBasket':
			send(content.mandant, content.value).then((rows) => {
				slack.webhook({
					text: 'Felix Mustermann fügte «' + rows[0].name + '» zum Warenkorb hinzu.'
				}, function (err, response) {
					console.log(response);
				});
			});
			break;

		case 'command.removeFromBasket':
			send(content.mandant, content.value).then((rows) => {
				slack.webhook({
					text: 'Felix Mustermann entfernte «' + rows[0].name + '» vom Warenkorb.'
				}, function (err, response) {
					console.log(response);
				});
			});
			break;
	}
});
