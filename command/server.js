'use static';

const express = require('express');
const bodyParser = require('body-parser');
const db = require('../modules/database');
const bus = require('../modules/event-bus');

const port = process.env.COMMAND_SERVICE_PORT || 8020;
const router = express.Router();

router.post('/add/product', (req, res) => {
  let mandant = req.headers['mandant-id'];
  let command = 'command.addProduct';
  let value = req.body.name;

  db.query('INSERT INTO events (mandant_id, type, value) VALUES (?, ?, ?)', [mandant, command, value]).then((rows) => {
    bus.publish(command, { mandant: mandant, value: value });
  });

  res.send('ok');
})

.delete('/remove/product', (req, res) => {
  let mandant = req.headers['mandant-id'];
  let command = 'command.removeProduct';
  let value = req.body.name;

  db.query('INSERT INTO events (mandant_id, type, value) VALUES (?, ?, ?)', [mandant, command, value]).then((rows) => {
    bus.publish(command, { mandant: mandant, value: value });
  });

  res.send('ok');
})

.post('/add/to/basket', (req, res) => {
  let mandant = req.headers['mandant-id'];
  let command = 'command.addToBasket';
  let value = req.body.id;

  db.query('INSERT INTO events (mandant_id, type, value) VALUES (?, ?, ?)', [mandant, command, value]).then((rows) => {
    bus.publish(command, { mandant: mandant, value: value });
  });

  res.send('ok');
})

.delete('/undo/basket', (req, res) => {
  let mandant = req.headers['mandant-id'];
  let command = 'command.undoBasket';

  db.query('DELETE FROM events WHERE mandant_id = ? ORDER BY id DESC LIMIT 1', [mandant]).then((rows) => {
    bus.publish(command, { mandant: mandant });
  });

  res.send('ok');
})

.delete('/remove/from/basket', (req, res) => {
  let mandant = req.headers['mandant-id'];
  let command = 'command.removeFromBasket';
  let value = req.body.id;

  db.query('INSERT INTO events (mandant_id, type, value) VALUES (?, ?, ?)', [mandant, command, value]).then((rows) => {
    bus.publish(command, { mandant: mandant, value: value });
  });

  res.send('ok');
});

express()
.use(bodyParser.json())
.use('/command', router)
.listen(port, function () {
  console.log('Command server listening on port %s.', port);
});