'use strict';

var http = require('http');
var httpProxy = require('http-proxy');

// console.log(process.env);

var staticHost = process.env.STATIC_SERVICE_HOST || 'localhost';
var commandHost = process.env.COMMAND_SERVICE_HOST || 'localhost';
var queryHost = process.env.QUERY_SERVICE_HOST || 'localhost';
var websocketHost = process.env.WEBSOCKET_SERVICE_HOST || 'localhost';

var proxyPort = process.env.PROXY_SERVICE_PORT || 8000;
var staticPort = process.env.STATIC_SERVICE_PORT || 8080;
var commandPort = process.env.COMMAND_SERVICE_PORT || 8020;
var queryPort = process.env.QUERY_SERVICE_PORT || 8030;
var websocketPort = process.env.WEBSOCKET_SERVICE_PORT || 8040;

var proxyStatic = new httpProxy.createServer({
  target: {
    host: staticHost,
    port: staticPort
  }
});

var proxyCommand = new httpProxy.createServer({
  target: {
    host: commandHost,
    port: commandPort
  }
});

var proxyQuery = new httpProxy.createServer({
  target: {
    host: queryHost,
    port: queryPort
  }
});

var proxyWebsocket = new httpProxy.createServer({
  ws: true,
  target: {
    host: websocketHost,
    port: websocketPort
  }
});

http.createServer(function (req, res) {
  req.headers['mandant-id'] = 2;

  switch (0) {
    case req.url.indexOf('/command'):
      proxyCommand.web(req, res);
      break;

    case req.url.indexOf('/query'):
      proxyQuery.web(req, res);
      break;

    default:
      proxyStatic.web(req, res);
      break;
  }
})

.on('upgrade', (req, socket, head) => {
  proxyWebsocket.ws(req, socket, head);
})

.listen(proxyPort, () => {
  console.log('Proxy server listening on port %s.', proxyPort);
});