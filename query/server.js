'use static';

let express = require('express');
let db = require('../modules/database');
let bus = require('../modules/event-bus');

const port = process.env.QUERY_SERVICE_PORT || 8030;
const router = express.Router();

router.get('/product', (req, res) => {
  let mandant = req.headers['mandant-id'];

  db.query('SELECT * FROM product').then((rows) => {
    res.send(rows);
  });
})

.get('/basket', (req, res) => {
  let mandant = req.headers['mandant-id'];

  db.query('SELECT * FROM basket WHERE mandant_id = ?', [mandant]).then((rows) => {
    res.send(rows);
  });
});

express()
  .use('/query', router)
  .listen(port, function () {
    console.log('Query server listening on port %s.', port);
  });