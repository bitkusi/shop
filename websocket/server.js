'use strict';

let WebSocketServer = require('ws').Server;

class EventHandler extends WebSocketServer {

	constructor() {
		var port = process.env.WEBSOCKET_SERVICE_PORT || 8040;
		super({ port: port });

		console.log('Websocket server listening on port %s.', port);

		let bus = require('../modules/event-bus');
		bus.subscribe('app.*', this.broadcast.bind(this));

		this.on('connection', this.connection);
	}

	connection(ws) {
		this.ws = ws;

		ws.on('open', () => {
			console.log('open');
		});

		ws.on('close', () => {
			console.log('close');
		});

		ws.on('message', message => {
			let event = JSON.parse(message);
			let data = event.data;

			self.broadcast(message);
		});
	}

	send(key, content) {
		let obj = {
			key: key,
			content: content
		};

		this.ws.send(JSON.stringify(obj));
	}

	// Broadcast to all.
	broadcast(key, content) {
		this.clients.forEach(client => {
			let obj = {
				key: key,
				content: content
			};

			client.send(JSON.stringify(obj));
		});
	}
}

module.exports = new EventHandler();
