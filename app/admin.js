(function (root) {
  'use strict';

  class Admin {

    constructor() {
      this.initWebSocket();
      this.loadProductList();
    }

    initWebSocket() {
      let url = 'ws://' + location.host;
      let connection = new WebSocket(url);

      connection.onmessage = (e) => {
        console.log('Server: ' + e.data);
        let data = JSON.parse(e.data);

        switch (data.key) {
          case 'app.addProduct':
          case 'app.removeProduct':
            this.loadProductList();
            break;
        }
      };
    }

    getHttpRequest(method, url) {
      let xhr = new XMLHttpRequest();
      xhr.open(method, url, true);
      xhr.setRequestHeader('Content-Type', 'application/json');
      return xhr;
    }

    add(name) {
      let xhr = this.getHttpRequest('POST', '/command/add/product');
      let data = JSON.stringify({ name: name.value });
      xhr.send(data);
      name.value = '';
      return false;
    }

    remove(name) {
      let xhr = this.getHttpRequest('DELETE', '/command/remove/product');
      let data = JSON.stringify({ name: name });
      xhr.send(data);
    }

    productListHandler() {
      let products = JSON.parse(this.responseText);
      let row = '';

      products.forEach(function (element) {
        row += '<li>';
        row += '<span class="name">' + element.name + '</span>';
        row += '<button onclick="admin.remove(\'' + element.name + '\')">X</button>';
        row += '</li>';
      });

      let list = document.getElementById('product-list');
      list.innerHTML = row;
    }

    loadProductList() {
      let xhr = this.getHttpRequest('GET', '/query/product');
      xhr.onload = this.productListHandler;
      xhr.send();
    }
  }

  root.admin = new Admin();

})(this);