(function (root) {
  'use strict';

  class Shop {

    constructor() {
      this.timer = null;

      this.initWebSocket();
      this.loadProductList();
      this.loadShoppingCart();
    }

    initWebSocket() {
      let url = 'ws://' + location.host;
      let connection = new WebSocket(url);

      connection.onmessage = (e) => {
        console.log('Server: ' + e.data);
        let data = JSON.parse(e.data);

        switch (data.key) {
          case 'app.addProduct':
          case 'app.removeProduct':
            this.loadProductList();
            break;
          
          case 'app.addToBasket':
            this.openToast('In den Warenkorb gelegt. <a href="javascript:shop.undo()">Rückgängig</a>');
            this.loadShoppingCart();
            break;

          case 'app.removeFromBasket':
            this.openToast('Vom Warenkorb entfernt. <a href="javascript:shop.undo()">Rückgängig</a>');
            this.loadShoppingCart();
            break;

          case 'app.undoBasket':
            this.loadShoppingCart();
            break;
        }
      };
    }

    getHttpRequest(method, url) {
      let xhr = new XMLHttpRequest();
      xhr.open(method, url, true);
      xhr.setRequestHeader('Content-Type', 'application/json');
      return xhr;
    }

    openToast(text) {
      let toast = document.getElementById('toast');
      toast.innerHTML = text;
      toast.classList.add("visible");

      this.timer = setTimeout(this.closeToast, 5000);
    }

    closeToast() {
      clearTimeout(this.timer);

      let toast = document.getElementById('toast');
      toast.classList.remove("visible");
    }

    add(id) {
      let xhr = this.getHttpRequest('POST', '/command/add/to/basket');
      let data = JSON.stringify({ id: id });
      xhr.send(data);
    }

    remove(id) {
      let xhr = this.getHttpRequest('DELETE', '/command/remove/from/basket');
      let data = JSON.stringify({ id: id });
      xhr.send(data);
    }

    undo() {
      this.closeToast();
      let xhr = this.getHttpRequest('DELETE', '/command/undo/basket');
      xhr.send();
    }

    productListHandler() {
      let products = JSON.parse(this.responseText);
      let row = '';

      products.forEach(function (element) {
        row += '<li>';
        row += '<span class="name">' + element.name + '</span>';
        row += '<button onclick="shop.add(' + element.id + ')">>></button>';
        row += '</li>';
      });

      let list = document.getElementById('product-list');
      list.innerHTML = row;
    }

    shoppingCartHandler() {
      let products = JSON.parse(this.responseText);
      let row = '';

      products.forEach(function (element) {
        row += '<li>';
        row += '<span class="quantity">' + element.quantity + '</span>';
        row += '<span class="name">' + element.name + '</span>';
        row += '<button onclick="shop.remove(' + element.product_id + ')">X</button>';
        row += '</li>';
      });

      let cart = document.getElementById('shopping-cart');
      cart.innerHTML = row;
    }

    loadProductList() {
      let xhr = this.getHttpRequest('GET', '/query/product');
      xhr.onload = this.productListHandler;
      xhr.send();
    }

    loadShoppingCart() {
      let xhr = this.getHttpRequest('GET', '/query/basket');
      xhr.onload = this.shoppingCartHandler;
      xhr.send();
    }
  }

  root.shop = new Shop();

})(this);