'use static';

let express = require('express');
let db = require('../modules/database');
let bus = require('../modules/event-bus');

function notify(key, content) {

  switch (key) {
    case 'command.addProduct':
      bus.publish('app.addProduct', content);
      break;

    case 'command.removeProduct':
      bus.publish('app.removeProduct', content);
      break;

    case 'command.addToBasket':
      bus.publish('app.addToBasket', { id: content.value });
      break;

    case 'command.removeFromBasket':
      bus.publish('app.removeFromBasket', { id: content.value });
      break;

    case 'command.undoBasket':
      bus.publish('app.undoBasket');
      break;
  }
}

function reset(key) {

  let promises = [];

  let promise = db.query('DELETE FROM product');
  promises.push(promise);

  promise = db.query('DELETE FROM basket');
  promises.push(promise);

  return Promise.all(promises);
}

function project(key, content) {

  let product = {};
  let basket = {};

  return db.query('SELECT type, value FROM events WHERE mandant_id = ? ORDER BY id',
    [content.mandant]).then((rows) => {

      rows.forEach((row) => {
        console.log(row);

        switch (row.type) {
          case 'command.addProduct':
            product[row.value] = row.value;
            break;

          case 'command.removeProduct':
            product[row.value] = null;
            break;

          case 'command.addToBasket':
            basket[row.value] = basket[row.value] === undefined ? 1 : basket[row.value] + 1;
            break;

          case 'command.removeFromBasket':
            basket[row.value] = 0;
            break;
        }
      });

      let promises = [];

      for (let i in product) {
        if (product[i]) {
          let promise = db.query('CALL add_product(?)', [product[i]]);
          promises.push(promise);
        }
        else {
          let promise = db.query('CALL remove_product(?)', [i]);
          promises.push(promise);
        }
      }

      for (let i in basket) {
        if (basket[i]) {
          let promise = db.query('CALL add_to_basket(?, ?, ?)', [content.mandant, i, basket[i]]);
          promises.push(promise);
        }
        else {
          let promise = db.query('CALL remove_from_basket(?, ?)', [content.mandant, i]);
          promises.push(promise);
        }
      }

      return Promise.all(promises);
    });
}

bus.subscribe('command.*', (key, content) => {

  //  reset(key).then(() => {
  project(key, content).then(() => {
    notify(key, content);
  });
  //  });

});
